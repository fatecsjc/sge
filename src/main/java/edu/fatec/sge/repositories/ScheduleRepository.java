package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Schedule;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ScheduleRepository extends CrudRepository<Schedule, Long> {
    List<Schedule> findAll();
    Schedule findById(Long id);

    @Query("SELECT s from Schedule s JOIN s.courseDiscipline cd WHERE cd.courseId = :courseId AND cd.disciplineId = :disciplineId")
    List<Schedule> findAllByCourseAndDiscipline(@Param("courseId") Long courseId, @Param("disciplineId") Long disciplineId);

    @Query("SELECT s from Schedule s JOIN s.courseDiscipline cd WHERE cd.courseId = :courseId AND cd.disciplineId = :disciplineId AND cd.professorId = :professorId")
    List<Schedule> findAllByCourseAndDisciplineAndProfessor(@Param("courseId") Long courseId,
                                                            @Param("disciplineId") Long disciplineId,
                                                            @Param("professorId") Long professorId);
}