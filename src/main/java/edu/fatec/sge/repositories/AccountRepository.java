package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Account;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface AccountRepository extends CrudRepository<Account, Long> {
    Account findByUsername(String username);
    Account findByCpfAndBirthday(String cpf, LocalDate birthday);
}
