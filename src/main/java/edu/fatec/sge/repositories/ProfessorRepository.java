package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Professor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProfessorRepository extends CrudRepository<Professor, Long> {
    List<Professor> findAll();
    Professor findById(Long id);
    Professor findByUsername(String username);
}
