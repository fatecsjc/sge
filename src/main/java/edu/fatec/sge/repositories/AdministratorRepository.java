package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Administrator;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AdministratorRepository extends CrudRepository<Administrator, Long> {
    List<Administrator> findAll();
    Administrator findById(Long id);
    Administrator findByUsername(String username);
}
