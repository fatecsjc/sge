package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Discipline;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DisciplineRepository extends CrudRepository<Discipline, Long> {
    Discipline findByName(String name);
    Discipline findByCode(String code);
    Discipline findById(Long id);

    @Query("SELECT d FROM Discipline d JOIN d.courseDisciplines cd WHERE cd.disciplineId = d.id AND cd.courseId = :id")
    List<Discipline> findByCourseId(@Param("id") Long id);

    @Query("SELECT d FROM Discipline d JOIN d.courseDisciplines cd WHERE cd.courseId = :courseId AND cd.disciplineId = :disciplineId")
    List<Discipline> findAllByCourseIdAndDisciplineId(@Param("courseId") Long courseId,
                                                      @Param("disciplineId") Long disciplineId);

    @Query("SELECT d FROM Discipline d JOIN d.courseDisciplines cd WHERE cd.courseId = :courseId AND cd.professorId = :professorId ")
    List<Discipline> findAllByCourseIdAndProfessorId(@Param("courseId") Long courseId,
                                                      @Param("professorId") Long professorId);

    @Query("SELECT d FROM Discipline d JOIN d.courseDisciplines cd WHERE cd.courseId = :courseId AND cd.disciplineId = :disciplineId AND cd.professorId = :professorId")
    Discipline findByCourseIdAndDisciplineIdAndProfessorId(@Param("courseId") Long courseId,
                                                     @Param("disciplineId") Long disciplineId,
                                                     @Param("professorId") Long professorId);

    List<Discipline> findAll();
}
