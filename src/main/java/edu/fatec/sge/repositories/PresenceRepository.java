package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Presence;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PresenceRepository extends CrudRepository<Presence , Long>{
    List<Presence> findAll();
    Presence findById(Long id);

    @Query("SELECT p FROM Presence p JOIN p.schedule s JOIN s.courseDiscipline cd WHERE cd.disciplineId = :disciplineId AND cd.professorId = :professorId")
    List<Presence> findAllByDisciplineAndProfessor(@Param("disciplineId") Long disciplineId, @Param("professorId") Long professorId);


}
