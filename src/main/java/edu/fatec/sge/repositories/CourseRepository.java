package edu.fatec.sge.repositories;

import edu.fatec.sge.models.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CourseRepository extends CrudRepository<Course, Long> {
    Course findByName(String name);
    Course findById(Long id);

    @Query("select c from Course c JOIN c.courseDisciplines cd WHERE cd.courseId = c.id AND cd.disciplineId = :id")
    List<Course> findByDisciplineId(@Param("id") Long id);

    List<Course> findAll();

    @Query("SELECT c from Course c JOIN c.courseDisciplines cd WHERE cd.professorId = :professorId")
    List<Course> findAllByProfessor(@Param("professorId") Long professorId);
}
