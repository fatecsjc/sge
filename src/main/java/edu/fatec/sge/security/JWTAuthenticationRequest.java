package edu.fatec.sge.security;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class JWTAuthenticationRequest implements Serializable {
    private static final long serialVersionUID = -8445943548965154778L;

    private String username;
    private String password;

    public JWTAuthenticationRequest() {
        super();
    }

}
