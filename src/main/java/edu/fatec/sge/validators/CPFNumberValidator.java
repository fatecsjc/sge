package edu.fatec.sge.validators;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class CPFNumberValidator implements
        ConstraintValidator<CPFNumber, String> {

    @Override
    public void initialize(CPFNumber cpfNumberConstraint) {
        /* This method needs to be overwrite for ConstraintValidator */
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        /* http://www.receita.fazenda.gov.br/aplicacoes/atcta/cpf/funcoes.js */
        int sum = 0;
        int rest;

        for (int i = 0; i < 10; i++) {
            if (Objects.equals(s, StringUtils.repeat(String.valueOf(i), 11))) {
                return false;
            }
        }

        if (s == null
                || s.length() < 11
                || !NumberUtils.isDigits(s))
            return false;

        for (int i=1; i <= 9; i++)
            sum = sum + Integer.parseInt((s.substring(i-1, i))) * (11 - i);
        rest = (sum * 10) % 11;

        if (rest == 10)
            rest = 0;
        if (rest != Integer.parseInt(s.substring(9, 10)))
            return false;

        sum = 0;
        for (int i = 1; i <= 10; i++) {
            sum = sum + Integer.parseInt((s.substring(i - 1, i))) * (12 - i);
        }

        rest = (sum * 10) % 11;
        if (rest == 10)
            rest = 0;

        return rest == Integer.parseInt(s.substring(10, 11));
    }
}
