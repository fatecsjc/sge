package edu.fatec.sge.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = PastLocalDateValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface PastLocalDate {
    int minAge() default 0;
    int maxAge() default 200;
    String message() default "invalid past date";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}