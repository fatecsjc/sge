package edu.fatec.sge.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CPFNumberValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface CPFNumber {
    String message() default "invalid CPF number";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}