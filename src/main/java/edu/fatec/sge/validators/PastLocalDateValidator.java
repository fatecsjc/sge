package edu.fatec.sge.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class PastLocalDateValidator implements
        ConstraintValidator<PastLocalDate, LocalDate> {

    private int minAge;
    private int maxAge;

    @Override
    public void initialize(PastLocalDate pastLocalDateConstraint) {
        this.minAge = pastLocalDateConstraint.minAge();
        this.maxAge = pastLocalDateConstraint.maxAge();
    }

    @Override
    public boolean isValid(LocalDate s, ConstraintValidatorContext constraintValidatorContext) {
        // Null is allowed
        if (s == null)
            return true;

        LocalDate transformedDate = s.withDayOfMonth(1).withMonth(1);
        LocalDate today = LocalDate.now().withDayOfMonth(1).withMonth(1);

        return (transformedDate.isBefore(today.minusYears(this.minAge))
                || transformedDate.isEqual(today.minusYears(this.minAge)))
                && (transformedDate.isAfter(today.minusYears(this.maxAge)));
    }
}
