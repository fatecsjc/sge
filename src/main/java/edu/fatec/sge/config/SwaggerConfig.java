package edu.fatec.sge.config;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static com.google.common.collect.Lists.*;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	SecurityConfiguration security() {
		return new SecurityConfiguration(
			null, // clientID
			null, // clientSecret
			null, // realm
			null, // appName
			null, // apiKey
			ApiKeyVehicle.HEADER, // apiKeyVehicle
			"Authorization", //apiKeyName
			"," // scopeSeparator
		);
	}

	private ApiKey apiKeys() {
		return new ApiKey("Authorization", "Authorization", "header");
	}

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
					.apis(RequestHandlerSelectors.basePackage("edu.fatec.sge.rest"))
					.paths(regex("/.*"))
				.build()
				.apiInfo(metaData())
				.securitySchemes(newArrayList(apiKeys()));
	}

	private ApiInfo metaData() {
		return new ApiInfoBuilder()
				.title("Sistema de Gerenciamento Escolar")
				.description("SGE REST API")
				.termsOfServiceUrl("")
				.contact(
						new Contact("Felipe Koblinger, Hamilton Zanini, João Matheus, Luis Bonfa, Robson Sousa",
								"", ""))
				.license("Apache License Version 2.0")
				.licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
				.build();
	}
}
