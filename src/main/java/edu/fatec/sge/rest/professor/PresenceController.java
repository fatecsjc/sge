package edu.fatec.sge.rest.professor;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import edu.fatec.sge.models.*;
import edu.fatec.sge.repositories.PresenceRepository;
import edu.fatec.sge.repositories.ScheduleRepository;
import edu.fatec.sge.repositories.StudentRepository;
import edu.fatec.sge.security.JWTAccount;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@RestController("ProfessorPresenceController")
@RequestMapping("/professor/presences/")
public class PresenceController {
    private PresenceRepository presenceRepository;
    private StudentRepository studentRepository;
    private ScheduleRepository scheduleRepository;

    public PresenceController(ScheduleRepository scheduleRepository,
                              PresenceRepository presenceRepository,
                              StudentRepository studentRepository) {
        this.scheduleRepository = scheduleRepository;
        this.studentRepository = studentRepository;
        this.presenceRepository = presenceRepository;
    }

    // POST: /professor/presences/ { student_id: 1, schedule_id: 2 }
    @ApiOperation(value = "Create one Presence", response = Student.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Successfully created Presence.")})
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<Presence> create(@RequestBody String jsonString) {
        JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
        Long studentId = jsonObject.get("studentId").getAsLong();
        Long scheduleId = jsonObject.get("scheduleId").getAsLong();

        Student student = studentRepository.findById(studentId);
        Schedule schedule = scheduleRepository.findById(scheduleId);

        Presence presence = new Presence();
        presence.setStudent(student);
        presence.setSchedule(schedule);
        presence.setDate(LocalDate.now());

        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!Objects.equals(presence.getSchedule().getCourseDiscipline().getProfessorId(), professor.getId())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        presenceRepository.save(presence);
        return (presence.getId() != 0) ? new ResponseEntity<>(presence, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // GET: /professor/presences/disciplines/1/
    @ApiOperation(value = "Get students presences of discipline ID", response = Grade.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Sucessfully retrieved students."),
            @ApiResponse(code = 404, message = "The resource was not found")})
    @RequestMapping(value = "/disciplines/{disciplineId}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Presence>> showStudentsPresencesDiscipline(@PathVariable Long disciplineId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Presence> presences = presenceRepository.findAllByDisciplineAndProfessor(disciplineId, professor.getId());

        return (presences != null) ? new ResponseEntity<>(presences, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    // DELETE: /professor/presences/1
    @ApiOperation(value = "Remove one Presence by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Presence deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the Presence")})
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Presence presence = presenceRepository.findById(id);

            if (!Objects.equals(presence.getSchedule().getCourseDiscipline().getProfessorId(), professor.getId())) {
                return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
            }

            presenceRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

}
