package edu.fatec.sge.rest.professor;

import edu.fatec.sge.models.CourseDiscipline;
import edu.fatec.sge.models.Grade;
import edu.fatec.sge.models.Professor;
import edu.fatec.sge.models.Student;
import edu.fatec.sge.repositories.CourseDisciplineRepository;
import edu.fatec.sge.repositories.GradeRepository;
import edu.fatec.sge.repositories.StudentRepository;
import edu.fatec.sge.security.JWTAccount;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController("GradeController")
@RequestMapping("/professor/grades/")
public class GradeController {
    private GradeRepository gradeRepository;
    private StudentRepository studentRepository;
    private CourseDisciplineRepository courseDisciplineRepository;

    public GradeController(CourseDisciplineRepository courseDisciplineRepository,
                           GradeRepository gradeRepository,
                           StudentRepository studentRepository) {
        this.courseDisciplineRepository = courseDisciplineRepository;
        this.studentRepository = studentRepository;
        this.gradeRepository = gradeRepository;
    }

    // POST -> /professor/grades/ { student_id: 3, course_discipline_id: 1, exam_one: 10.0, exam_two: 9 }
    @ApiOperation(value = "Create one Grade", response = Student.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Successfully created Grade.")})
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<Grade> create(@RequestBody Grade grade) {
        Student student = studentRepository.findById(grade.getStudentId());
        CourseDiscipline courseDiscipline = courseDisciplineRepository.findById(grade.getCourseDisciplineId());
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!Objects.equals(courseDiscipline.getProfessorId(), professor.getId())) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        grade.setStudent(student);
        grade.setCourseDiscipline(courseDiscipline);

        gradeRepository.save(grade);
        return (grade.getId() != 0) ? new ResponseEntity<>(grade, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // PUT -> /professor/grades/:id { exam_one: 10.0, exam_two: 9 }
    @ApiOperation(value = "Update one Grade", response = Professor.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Successfully updated grade.")})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResponseEntity<Grade> update(@RequestBody Grade grade, @PathVariable Long id) {
        Grade queryGrade = gradeRepository.findById(id);

        queryGrade.setExamOne(grade.getExamOne());
        queryGrade.setExamTwo(grade.getExamTwo());

        gradeRepository.save(queryGrade);

        return (queryGrade.getId() != 0) ? new ResponseEntity<>(queryGrade, HttpStatus.ACCEPTED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // DELETE -> /professor/grades/:id
    @ApiOperation(value = "Remove one Grade by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Grade deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the Grade")})
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            gradeRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    // GET -> /professor/grades/disciplines/:id/students/:id {Discipline: 2 - Student: 3}
    @ApiOperation(value = "Get grades from Students by Discipline", response = Grade.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Sucessfully retrieved grades."),
            @ApiResponse(code = 404, message = "The resource was not found")})
    @RequestMapping(value = "/disciplines/{disciplineId}/students/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Grade>> showGradesStudentsByDiscipline(@PathVariable Long disciplineId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Grade> grades = gradeRepository.findAllStudentsByDiscipline(disciplineId, professor.getId());

        return (grades != null) ? new ResponseEntity<>(grades, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // GET -> /professor/grades/disciplines/:id/students/:id {Discipline: 2 - Student: 3}
    @ApiOperation(value = "Get grades from Student by Discipline", response = Grade.class)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Sucessfully retrieved grades."),
            @ApiResponse(code = 404, message = "The resource was not found")})
    @RequestMapping(value = "/disciplines/{disciplineId}/students/{studentId}", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Grade>> showGradesByStudentAndByDiscipline(@PathVariable Long disciplineId, @PathVariable Long studentId) {
        JWTAccount professor = (JWTAccount) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Grade> grades = gradeRepository.findByStudentAndDiscipline(disciplineId, studentId, professor.getId());

        return (grades != null) ? new ResponseEntity<>(grades, HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
