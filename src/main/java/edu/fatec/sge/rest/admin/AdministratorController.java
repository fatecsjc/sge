package edu.fatec.sge.rest.admin;

import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;
import edu.fatec.sge.models.Course;
import edu.fatec.sge.utils.NullAwareBeanUtilsBean;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.fatec.sge.models.Administrator;
import edu.fatec.sge.repositories.AdministratorRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@RestController("AdminAdministratorController")
@RequestMapping("/admin/administrators/")
public class AdministratorController {
	private AdministratorRepository administratorRepository;
	
	@Autowired
    public AdministratorController(AdministratorRepository administratorRepository) {
        this.administratorRepository = administratorRepository;
    }

    // GET: /admin/administrators/
    @ApiOperation(value = "Query all administrators stored in database", response = Course.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list") })
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Administrator>> index() {
        return new ResponseEntity<>(administratorRepository.findAll(), HttpStatus.OK);
    }

	@ApiOperation(value = "Get one administrator base on its ID or Username", response = Administrator.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved administrator"),
    		@ApiResponse(code = 404, message = "The resource was not found")})
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Administrator> show(@PathVariable String id) {
        Administrator administrator;
        if (NumberUtils.isNumber(id)) {
            administrator = administratorRepository.findById(Long.parseLong(id));
        } else {
            administrator = administratorRepository.findByUsername(id);
        }
        return (administrator != null) ? new ResponseEntity<>(administrator, HttpStatus.OK)
        		: new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
    @ApiOperation(value = "Create one Administrator", response = Administrator.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created Administrator") })
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Administrator> create(@RequestBody Administrator admin) {
        Authority authority = new Authority();
        authority.setName(AuthorityName.ROLE_ADMIN);
        authority.setId(1L);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authority);
        admin.setAuthorities(authorities);

        administratorRepository.save(admin);
        return (admin.getId() != 0) ? new ResponseEntity<>(admin, HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @ApiOperation(value = "Update one Administrator", response = Administrator.class)
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully updated Administrator") })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE })
    public ResponseEntity<Administrator> update(@RequestBody Administrator admin, @PathVariable Long id) {
    	Administrator queryAdmin = administratorRepository.findById(id);

        BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
        try {
            notNull.copyProperties(queryAdmin, admin);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        administratorRepository.save(queryAdmin);
        return (queryAdmin.getId() != 0) ? new ResponseEntity<>(queryAdmin, HttpStatus.ACCEPTED)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @ApiOperation(value = "Remove one Administrator by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Administrator deleted with success"),
            @ApiResponse(code = 406, message = "Fail to delete the Administrator") })
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            administratorRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }
    }
}