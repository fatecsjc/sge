package edu.fatec.sge.models;

public enum GenreName {
    MALE, FEMALE;
}
