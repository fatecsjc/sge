package edu.fatec.sge.models;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import edu.fatec.sge.validators.CPFNumber;
import edu.fatec.sge.validators.PastLocalDate;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "accounts")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=Account.class)
@DynamicUpdate
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Email
    @Column(unique = true)
    private String email;

    @NotNull
    @Size(min = 4, max = 50)
    @Column(unique = true)
    private String username;

    @NotNull
    @Size(max = 60)
    private String password;

    @NotNull
    @Size(min = 3, max = 100)
    private String name;

    @CPFNumber
    @Column(unique = true)
    private String cpf;

    @NotNull
    @Size(min = 5, max = 15)
    private String rg;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @PastLocalDate(minAge = 10, maxAge = 120)
    private LocalDate birthday;

    @NotNull
    @Enumerated(EnumType.STRING)
    private GenreName genre;

    @JsonIgnore
    @Column(name = "last_password_reset_date")
    private Date lastPasswordResetDate;

    @JsonIgnore
    @ManyToMany
    @JoinTable(name = "account_authorities",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "authority_id"))
    @NotNull
    @Size(min = 1, max = 1)
    private List<Authority> authorities;

    /* Public Methods */
    public void setPassword(String password) {
        this.password = password;
        if (this.password != null)
            this.password = new BCryptPasswordEncoder().encode(password);
    }

}
