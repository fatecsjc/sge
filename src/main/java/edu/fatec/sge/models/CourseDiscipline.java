package edu.fatec.sge.models;
import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "course_disciplines")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=CourseDiscipline.class)
public class CourseDiscipline {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "discipline_id", insertable = false, updatable = false)
    private Long disciplineId;
    @Column(name = "course_id", insertable = false, updatable = false)
    private Long courseId;
    @Column(name = "professor_id", insertable = false, updatable = false)
    private Long professorId;

    @NotNull
    private Integer semester;
    
    @NotNull
    private Integer year;
    
    @NotNull
    private Integer workload;

    @ManyToOne
    @JoinColumn(name="course_id")
    private Course course;

    @ManyToOne
    @JoinColumn(name="discipline_id")
    private Discipline discipline;

    @ManyToOne
    @JoinColumn(name="professor_id")
    private Professor professor;

    @JsonIgnore
    @ManyToMany(mappedBy = "courseDisciplines")
    private Set<Student> students;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "course_discipline_id")
    private Set<Schedule> schedules;
}