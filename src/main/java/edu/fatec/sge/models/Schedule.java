package edu.fatec.sge.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Time;

@Getter
@Setter
@Entity
@Table(name = "schedules")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=Schedule.class)
public class Schedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    private ScheduleWeekdayName weekday;

    @NotNull
    @Column(name = "time_start")
    private Time timeStart;

    @NotNull
    @Column(name = "time_end")
    private Time timeEnd;

    @NotNull
    private Integer quantity;

    @ManyToOne
    @JoinColumn(name="course_discipline_id")
    private CourseDiscipline courseDiscipline;
}
