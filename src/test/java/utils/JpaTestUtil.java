package utils;

import org.springframework.data.repository.CrudRepository;

public class JpaTestUtil {
    public static void deleteRepositories(CrudRepository... repositories) {
        for(CrudRepository repository: repositories) {
            repository.deleteAll();
        }
    }
}
