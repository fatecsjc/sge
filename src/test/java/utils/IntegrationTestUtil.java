package utils;

import com.google.gson.JsonObject;
import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.Administrator;
import edu.fatec.sge.models.Professor;
import edu.fatec.sge.models.Student;
import edu.fatec.sge.repositories.AccountRepository;
import edu.fatec.sge.repositories.AdministratorRepository;
import edu.fatec.sge.repositories.ProfessorRepository;
import edu.fatec.sge.repositories.StudentRepository;
import factories.AdministratorFactory;
import factories.ProfessorFactory;
import factories.StudentFactory;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
@Ignore
public class IntegrationTestUtil {
    @LocalServerPort
    private int port;

    private String token;

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    AdministratorRepository administratorRepository;
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    ProfessorRepository professorRepository;

    @Before
    public void setupBefore() {
        RestAssured.port = port;
    }

    @BeforeClass
    public static void setup() {
        String basePath = System.getProperty("server.base");
        if (basePath == null) {
            basePath = "/api/v1/";
        }
        RestAssured.basePath = basePath;

        String baseHost = System.getProperty("server.host");
        if (baseHost == null) {
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;
    }

    public void setToken(String role) {
        JsonObject user = new JsonObject();

        switch(role) {
            case "ROLE_STUDENT":
                user.addProperty("username", "afonsomikhael");
                user.addProperty("password", "amika99#");
                break;
            case "ROLE_PROFESSOR":
                user.addProperty("username", "florindacarina");
                user.addProperty("password", "floralinda11");
                break;
            case "ROLE_ADMIN":
            default:
                user.addProperty("username", "arabella");
                user.addProperty("password", "arabella80#");
        }

        Response r =
                given()
                    .contentType("application/json")
                    .body(user.toString())
                .when()
                    .post("/auth/");
        this.token = r.getBody().jsonPath().get("token").toString();
    }

    public String getToken() {
        return this.token;
    }
}