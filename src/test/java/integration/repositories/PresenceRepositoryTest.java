package integration.repositories;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import edu.fatec.sge.repositories.*;
import factories.CourseDisciplineFactory;
import factories.CourseFactory;
import factories.DisciplineFactory;
import factories.PresenceFactory;
import factories.ScheduleFactory;
import factories.StudentFactory;
import edu.fatec.sge.SpringMainApplication;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;
import edu.fatec.sge.models.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class PresenceRepositoryTest extends TestUtil {
	
	@Autowired
	PresenceRepository presenceRepository;
	
	@Autowired
	StudentRepository studentRepository;
	@Autowired
	ScheduleRepository scheduleRepository;
	@Autowired
    CourseRepository courseRepository;
    @Autowired
    DisciplineRepository disciplineRepository;
    @Autowired
    CourseDisciplineRepository courseDisciplineRepository;

    public Presence persistedRelationships() {
    	Student student = studentRepository.save(StudentFactory.validStudent());
    	Course course = courseRepository.save(CourseFactory.validCourse());
    	Discipline discipline = disciplineRepository.save(DisciplineFactory.validDiscipline());
    	
    	CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();
    	courseDiscipline.setCourse(course);
    	courseDiscipline.setDiscipline(discipline);
    	courseDisciplineRepository.save(courseDiscipline);
    	
    	Schedule schedule = ScheduleFactory.validSchedule();
    	schedule.setCourseDiscipline(courseDiscipline);
    	scheduleRepository.save(schedule);
    	
    	Presence presence = PresenceFactory.validPresence();
    	presence.setStudent(student);
    	presence.setSchedule(schedule);
    	return presence;
    }
    
    @Test
    public void presenceShouldSave() {
    	Presence presence = presenceRepository.save(persistedRelationships());
    	
    	Presence savedPresence = presenceRepository.findOne(presence.getId());

    	assertEquals(presence.getId(), savedPresence.getId());
    }
    
    @Test
    public void presenceShouldBeUpdated() {
    	Presence presence = presenceRepository.save(persistedRelationships());
    	
    	presence.setDate(LocalDate.of(2000, 1, 1));
    	presenceRepository.save(presence);
    	
    	assertEquals(presence.getId(), presenceRepository.findOne(presence.getId()).getId());
    }
    

    @Test
    public void presenceShouldBeDeleted() {
    	Presence presence = persistedRelationships();
    	
    	presenceRepository.save(presence);
    	presenceRepository.delete(presence);
    	
    	assertEquals(null, presenceRepository.findOne(presence.getId()));
    }

	@Test
	public void studentShouldBeListed() {
		Presence presence = persistedRelationships();
		presenceRepository.save(presence);

		List<Presence> presences = presenceRepository.findAll();
		assertTrue(presences.size() > 0);
	}
}
