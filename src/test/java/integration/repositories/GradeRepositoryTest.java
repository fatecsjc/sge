package integration.repositories;

import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.*;
import edu.fatec.sge.repositories.*;
import factories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class GradeRepositoryTest extends TestUtil {

    @Autowired
    GradeRepository gradeRepository;

    @Autowired
    CourseRepository courseRepository;
    @Autowired
    DisciplineRepository disciplineRepository;
    @Autowired
    CourseDisciplineRepository courseDisciplineRepository;
    @Autowired
    StudentRepository studentRepository;

    public Grade persistedRelationships() {
        Course course = courseRepository.save(CourseFactory.validCourse());
        Discipline discipline = disciplineRepository.save(DisciplineFactory.validDiscipline());

        CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();
        courseDiscipline.setCourse(course);
        courseDiscipline.setDiscipline(discipline);
        courseDisciplineRepository.save(courseDiscipline);

        Student student = StudentFactory.validStudent();
        studentRepository.save(student);

        Grade grade = GradeFactory.validGrade();
        grade.setCourseDiscipline(courseDiscipline);
        grade.setStudent(student);
        return grade;
    }

    @Test
    public void gradeShouldSave() {
        Grade grade = persistedRelationships();
        gradeRepository.save(grade);

        Grade gradeSaved = gradeRepository.findByStudent(grade.getStudent());
        assertNotNull(gradeSaved);
    }

    @Test
    public void gradeShouldBeUpdated() {
        Grade grade = persistedRelationships();
        gradeRepository.save(grade);

        grade.setExamOne(new BigDecimal(10.0));
        gradeRepository.save(grade);

        Grade gradeUpdated = gradeRepository.findOne(grade.getId());
        assertTrue(grade.getExamOne().compareTo(gradeUpdated.getExamOne()) == 0);
    }

    @Test
    public void gradeShouldBeDeleted() {
        Grade grade = persistedRelationships();
        gradeRepository.save(grade);

        gradeRepository.delete(grade);
        Grade graveDeleted = gradeRepository.findOne(grade.getId());
        assertEquals(null, graveDeleted);
    }

    @Test
    public void gradeShouldBeListed() {
        Grade grade = persistedRelationships();
        gradeRepository.save(grade);

        List<Grade> grades = gradeRepository.findAll();
        assertTrue(grades.size() > 0);
    }
}
