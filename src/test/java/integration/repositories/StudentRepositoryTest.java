package integration.repositories;

import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.Student;
import edu.fatec.sge.repositories.*;
import factories.StudentFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;

import java.util.List;

import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class StudentRepositoryTest extends TestUtil {

    @Autowired
    StudentRepository studentRepository;

    @Test
    public void studentShouldSave() {
        Student student = studentRepository
                .save(StudentFactory.validStudent());

        Student studentSaved = studentRepository.findByUsername(student.getUsername());
        assertEquals(studentSaved.getName(), student.getName());
    }

    @Test
    public void studentShouldNotBeSavedWithDuplicates() {
        Student student = StudentFactory.validStudent();

        Student studentDuplicated = StudentFactory.validStudent();
        studentDuplicated.setEmail("joao@debarro.com.br");
        studentDuplicated.setUsername("joaodasneves2");
        studentDuplicated.setCpf("37157818582");
        studentDuplicated.setRa("1234569BDD");

        TestTransaction.flagForRollback();
        studentRepository.save(StudentFactory.validStudent());

        studentDuplicated.setEmail(student.getEmail());
        try {
            studentRepository.save(studentDuplicated);
            fail("Student `email` must not save with duplicated.");
        } catch (DataIntegrityViolationException ignored) {}
        TestTransaction.end();

        TestTransaction.start();
        TestTransaction.flagForRollback();
        studentRepository.save(StudentFactory.validStudent());

        studentDuplicated.setUsername(student.getUsername());
        try {
            studentRepository.save(studentDuplicated);
            fail("Student `username` must not save with duplicated.");
        } catch (DataIntegrityViolationException ignored) {}
        TestTransaction.end();

        TestTransaction.start();
        TestTransaction.flagForRollback();
        studentRepository.save(StudentFactory.validStudent());

        studentDuplicated.setCpf(student.getCpf());
        try {
            studentRepository.save(studentDuplicated);
            fail("Student `cpf` must not save with duplicated.");
        } catch (DataIntegrityViolationException ignored) {}
        TestTransaction.end();

        TestTransaction.start();
        TestTransaction.flagForRollback();
        studentRepository.save(StudentFactory.validStudent());

        studentDuplicated.setRa(student.getRa());
        try {
            studentRepository.save(studentDuplicated);
            fail("Student `ra` must not save with duplicated.");
        } catch (DataIntegrityViolationException ignored) {}
        TestTransaction.end();

        TestTransaction.start();
        TestTransaction.flagForRollback();
        studentDuplicated.setEmail("joao@debarro.com.br");
        studentDuplicated.setUsername("joaodasneves2");
        studentDuplicated.setCpf("37157818582");
        studentDuplicated.setRa("1234569BDD");
        Student studentSaved = studentRepository.save(studentDuplicated);
        TestTransaction.end();

        assertEquals(studentSaved.getName(), studentDuplicated.getName());
    }

    @Test
    public void studentShouldBeUpdated() {
        studentRepository.save(StudentFactory.validStudent());

        Student student = studentRepository.findByUsername("joaoneves");

        student.setRa("1231234LOG");
        studentRepository.save(student);

        Student studentUpdated = studentRepository.findByUsername(student.getUsername());
        assertEquals(studentUpdated.getRa(), student.getRa());
    }

    @Test
    public void studentShouldBeDeleted() {
        Student student = studentRepository
                .save(StudentFactory.validStudent());
        studentRepository.delete(student);
        Student studentDeleted = studentRepository.findByUsername(student.getUsername());
        assertEquals(null, studentDeleted);
    }

    @Test
    public void studentShouldBeListed() {
        studentRepository.save(StudentFactory.validStudent());

        List<Student> students = studentRepository.findAll();
        assertTrue(students.size() > 0);
    }
}