package integration.repositories;

import edu.fatec.sge.SpringMainApplication;
import edu.fatec.sge.models.*;
import edu.fatec.sge.repositories.*;
import factories.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import utils.TestUtil;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringMainApplication.class)
@Transactional
public class CourseDisciplineRepositoryTest extends TestUtil {

    @Autowired
    CourseDisciplineRepository courseDisciplineRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    DisciplineRepository disciplineRepository;

    public CourseDiscipline persistedRelationships() {
        Course course = courseRepository.save(CourseFactory.validCourse());
        Discipline discipline = disciplineRepository.save(DisciplineFactory.validDiscipline());

        CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();
        courseDiscipline.setCourse(course);
        courseDiscipline.setDiscipline(discipline);
        return courseDiscipline;
    }

    @Test
    public void courseDisciplineShouldSave() {
        CourseDiscipline courseDiscipline = persistedRelationships();
        courseDisciplineRepository.save(courseDiscipline);

        CourseDiscipline courseDisciplineSaved = courseDisciplineRepository.findOne(courseDiscipline.getId());
        assertNotNull(courseDisciplineSaved);
    }

    @Test
    public void courseDisciplineShouldBeUpdated() {
        CourseDiscipline courseDiscipline = persistedRelationships();
        courseDisciplineRepository.save(courseDiscipline);

        courseDiscipline.setWorkload(200);
        courseDisciplineRepository.save(courseDiscipline);

        CourseDiscipline gradeUpdated = courseDisciplineRepository.findOne(courseDiscipline.getId());
        assertEquals(courseDiscipline.getWorkload(), gradeUpdated.getWorkload());
    }

    @Test
    public void courseDisciplineShouldBeDeleted() {
        CourseDiscipline courseDiscipline = persistedRelationships();
        courseDisciplineRepository.save(courseDiscipline);

        courseDisciplineRepository.delete(courseDiscipline);
        CourseDiscipline gradeDeleted = courseDisciplineRepository.findOne(courseDiscipline.getId());
        assertEquals(null, gradeDeleted);
    }

    @Test
    public void courseDisciplineShouldBeListed() {
        CourseDiscipline courseDiscipline = persistedRelationships();
        courseDisciplineRepository.save(courseDiscipline);

        List<CourseDiscipline> courseDisciplines = courseDisciplineRepository.findAll();
        assertTrue(courseDisciplines.size() > 0);
    }
}
