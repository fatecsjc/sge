package factories;

import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;
import edu.fatec.sge.models.Professor;

import java.util.ArrayList;
import java.util.List;

public class ProfessorFactory {
    public static Professor validProfessor() {
        Professor validProfessor = new Professor();
        validProfessor = (Professor) AccountFactory.validAccount(validProfessor);
        validProfessor.setCpf("76216388690");
        validProfessor.setRg("309182939X");
        validProfessor.setEmail("professor@fatec.com.br");
        validProfessor.setUsername("professor");
        validProfessor.setPassword("1234567891");

        Authority authority = new Authority();
        authority.setName(AuthorityName.ROLE_PROFESSOR);
        authority.setId(3L);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(authority);
        validProfessor.setAuthorities(authorities);

        return validProfessor;
    }
}
