package factories;

import edu.fatec.sge.models.Discipline;

public class DisciplineFactory {

    public static Discipline validDiscipline(){
        Discipline validDiscipline = new Discipline();
        validDiscipline.setName("Estrutura de Dados");
        validDiscipline.setCode("estds");

        return validDiscipline;
    }
}
