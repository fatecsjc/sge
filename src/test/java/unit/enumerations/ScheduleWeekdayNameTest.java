package unit.enumerations;

import edu.fatec.sge.models.ScheduleWeekdayName;
import org.junit.Test;
import utils.TestUtil;

import static org.junit.Assert.assertEquals;

public class ScheduleWeekdayNameTest extends TestUtil {
    @Test
    public void scheduleWeekdayNameShouldBeValid() {
        /* Maximum 7 days */
        assertEquals(7, ScheduleWeekdayName.values().length);
        /* Week start in monday at value 0 until Sunday as value 6 */
        assertEquals(0, ScheduleWeekdayName.MONDAY.getValue());
        assertEquals(1, ScheduleWeekdayName.TUESDAY.getValue());
        assertEquals(2, ScheduleWeekdayName.WEDNESDAY.getValue());
        assertEquals(3, ScheduleWeekdayName.THURSDAY.getValue());
        assertEquals(4, ScheduleWeekdayName.FRIDAY.getValue());
        assertEquals(5, ScheduleWeekdayName.SATURDAY.getValue());
        assertEquals(6, ScheduleWeekdayName.SUNDAY.getValue());
    }
}
