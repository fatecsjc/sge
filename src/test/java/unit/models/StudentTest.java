package unit.models;

import edu.fatec.sge.models.Student;
import factories.StudentFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class StudentTest extends TestUtil {
    @Test
    public void studentTestPojo() {
        final Class<?> classUnderTest = Student.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void studentShouldBeValid() {
        Student validStudent = StudentFactory.validStudent();
        assertEquals( 0, getErrorSize(validStudent));
    }

    @Test
    public void studentShouldNotBeValidWithInvalidRa() {
        Student student = StudentFactory.validStudent();

        student.setRa(null);
        assertEquals( 1, getErrorSize(student));
        assertEquals("may not be null", getErrorMessage(student));

        student.setRa("1234567BD1");
        assertEquals( 1, getErrorSize(student));
        assertEquals("must match \"[0-9]{7}[a-zA-Z]{3}\"", getErrorMessage(student));
    }
}
