package unit.models;

import edu.fatec.sge.models.Account;
import edu.fatec.sge.models.Authority;
import edu.fatec.sge.models.AuthorityName;
import factories.AccountFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.FieldPredicate.exclude;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class AccountTest extends TestUtil {
    @Test
    public void accountTestPojo() {
        final Class<?> classUnderTest = Account.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest, exclude("password"))
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void accountShouldBeValid() {
        Account validAccount = AccountFactory.validAccount(new Account());
        assertEquals( 0, getErrorSize(validAccount));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidEmail() {
        Account account = AccountFactory.validAccount(new Account());
        account.setEmail("jajsdij@okkl.");
        assertEquals( 1, getErrorSize(account));
        assertEquals("not a well-formed email address", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidUsername() {
        Account account = AccountFactory.validAccount(new Account());

        account.setUsername(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("may not be null", getErrorMessage(account));

        account.setUsername("ola");
        assertEquals(1, getErrorSize(account));
        assertEquals("size must be between 4 and 50", getErrorMessage(account));

        account.setUsername("123456789012345678901234567890123456789012345678901");
        assertEquals(1, getErrorSize(account));
        assertEquals("size must be between 4 and 50", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidPassword() {
        Account account = AccountFactory.validAccount(new Account());

        account.setPassword(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("may not be null", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidName() {
        Account account = AccountFactory.validAccount(new Account());

        account.setName(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("may not be null", getErrorMessage(account));

        account.setName("12");
        assertEquals(1, getErrorSize(account));
        assertEquals("size must be between 3 and 100", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidCPF() {
        Account account = AccountFactory.validAccount(new Account());

        account.setCpf("12345677909");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf("12345678908");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf("12345678995");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf("00000000000");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf("123456789");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf("");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));

        account.setCpf("uma palavra qualquer");
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid CPF number", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidRg() {
        Account account = AccountFactory.validAccount(new Account());

        account.setRg(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("may not be null", getErrorMessage(account));

        account.setRg("1234567890123456");
        assertEquals( 1, getErrorSize(account));
        assertEquals("size must be between 5 and 15", getErrorMessage(account));
    }

    @Test
    public void accountShouldBeValidWithNulledBirthday() {
        Account account = AccountFactory.validAccount(new Account());

        account.setBirthday(null);
        assertEquals( 0, getErrorSize(account));
        assertEquals("is a valid object", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidBirthday() {
        Account account = AccountFactory.validAccount(new Account());

        account.setBirthday(LocalDate.now().minusYears(9));
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid past date", getErrorMessage(account));

        account.setBirthday(LocalDate.now().minusYears(121));
        assertEquals( 1, getErrorSize(account));
        assertEquals("invalid past date", getErrorMessage(account));
    }

    @Test
    public void accountShouldNotBeValidWithInvalidGenre() {
        Account account = AccountFactory.validAccount(new Account());
        account.setGenre(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("may not be null", getErrorMessage(account));

    }

    @Test
    public void accountShouldNotBeValidWithInvalidAuthority() {
        Account account = AccountFactory.validAccount(new Account());

        Authority studentAuthority = new Authority();
        studentAuthority.setName(AuthorityName.ROLE_STUDENT);
        studentAuthority.setId(2L);

        Authority teacherAuthority = new Authority();
        teacherAuthority.setName(AuthorityName.ROLE_PROFESSOR);
        teacherAuthority.setId(2L);

        List<Authority> anotherAuthorities = account.getAuthorities();
        anotherAuthorities.add(studentAuthority);
        anotherAuthorities.add(teacherAuthority);

        account.setAuthorities(anotherAuthorities);
        assertEquals( 1, getErrorSize(account));
        assertEquals("size must be between 1 and 1", getErrorMessage(account));

        account.setAuthorities(null);
        assertEquals( 1, getErrorSize(account));
        assertEquals("may not be null", getErrorMessage(account));
    }
}
