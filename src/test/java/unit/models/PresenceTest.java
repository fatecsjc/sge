package unit.models;

import edu.fatec.sge.models.*;
import factories.PresenceFactory;

import static org.junit.Assert.*;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

public class PresenceTest extends TestUtil {
    @Test
    public void presenceTestPojo() {
        final Class<?> classUnderTest = Presence.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void presenceShouldBeValid() {
        Presence validPresence = PresenceFactory.validPresence();
        assertEquals(0, getErrorSize(validPresence));
    }
}
