package unit.models;

import edu.fatec.sge.models.Authority;
import factories.AuthorityFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class AuthorityTest extends TestUtil {
    @Test
    public void authorityTestPojo() {
        final Class<?> classUnderTest = Authority.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void authorityShouldBeValid() {
        Authority validAuthority = AuthorityFactory.validAuthority();
        assertEquals( 0, getErrorSize(validAuthority));
    }

}
