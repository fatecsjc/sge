package unit.models;

import edu.fatec.sge.models.Professor;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class ProfessorTest extends TestUtil {
    @Test
    public void professorTestPojo() {
        final Class<?> classUnderTest = Professor.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }
}
