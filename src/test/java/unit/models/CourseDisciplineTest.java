package unit.models;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

import org.junit.Test;

import edu.fatec.sge.models.CourseDiscipline;
import factories.CourseDisciplineFactory;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

public class CourseDisciplineTest extends TestUtil {
    @Test
    public void courseDisciplineTestPojo() {
        final Class<?> classUnderTest = CourseDiscipline.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void courseDisciplineShouldBeValid() {
    	 CourseDiscipline validCourseDiscipline = CourseDisciplineFactory.validCourseDiscipline();
        assertEquals( 0, getErrorSize(validCourseDiscipline));
    }
    
    @Test
    public void courseDisciplineShouldNotBeValidWithInvalidSemester() {
        CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();

        courseDiscipline.setSemester(null);
        assertEquals( 1, getErrorSize(courseDiscipline));
        assertEquals("may not be null", getErrorMessage(courseDiscipline));      

    }
    
    @Test
    public void courseDisciplineShouldNotBeValidWithInvalidYear() {
        CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();

        courseDiscipline.setYear(null);
        assertEquals( 1, getErrorSize(courseDiscipline));
        assertEquals("may not be null", getErrorMessage(courseDiscipline));
       
    }
    
    @Test
    public void courseDisciplineShouldNotBeValidWithInvalidWorkload() {
        CourseDiscipline courseDiscipline = CourseDisciplineFactory.validCourseDiscipline();

        courseDiscipline.setWorkload(null);
        assertEquals( 1, getErrorSize(courseDiscipline));
        assertEquals("may not be null", getErrorMessage(courseDiscipline));
        

    }

    
}
