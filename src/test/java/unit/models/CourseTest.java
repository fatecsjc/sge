package unit.models;

import edu.fatec.sge.models.Course;
import factories.CourseFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class CourseTest extends TestUtil {
    @Test
    public void courseTestPojo() {
        final Class<?> classUnderTest = Course.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void courseShouldBeValid() {
        Course validCourse = CourseFactory.validCourse();
        assertEquals( 0, getErrorSize(validCourse));
    }

    @Test
    public void courseShouldNotBeValidWithInvalidName() {
        Course course = CourseFactory.validCourse();

        course.setName(null);
        assertEquals( 1, getErrorSize(course));
        assertEquals("may not be null", getErrorMessage(course));

        course.setName("BDD");
        assertEquals( 1, getErrorSize(course));
        assertEquals("size must be between 4 and 100", getErrorMessage(course));

        course.setName("Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco de Dados 1Banco");
        assertEquals( 1, getErrorSize(course));
        assertEquals("size must be between 4 and 100", getErrorMessage(course));

    }
}
