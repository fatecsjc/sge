package unit.models;

import edu.fatec.sge.models.Grade;
import factories.GradeFactory;
import org.junit.Test;
import pl.pojo.tester.api.assertion.Method;
import utils.TestUtil;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static pl.pojo.tester.api.assertion.Assertions.assertPojoMethodsFor;

public class GradeTest extends TestUtil {
    @Test
    public void gradeTestPojo() {
        final Class<?> classUnderTest = Grade.class;

        assertPojoMethodsFor(classUnderTest)
                .testing(Method.GETTER)
                .areWellImplemented();
        assertPojoMethodsFor(classUnderTest)
                .testing(Method.SETTER)
                .areWellImplemented();
    }

    @Test
    public void gradeShouldBeValid() {
        Grade validGrade = GradeFactory.validGrade();
        assertEquals( 0, getErrorSize(validGrade));
    }

    @Test
    public void gradeShouldNotBeValidWithInvalidExamOne() {
        Grade grade = GradeFactory.validGrade();
        grade.setExamOne(new BigDecimal(-10.00));
        assertEquals( 1, getErrorSize(grade));
        assertEquals("must be greater than or equal to 0.0", getErrorMessage(grade));

        grade.setExamOne(new BigDecimal(10.01));
        assertEquals( 1, getErrorSize(grade));
        assertEquals("must be less than or equal to 10.0", getErrorMessage(grade));

        grade.setExamOne(null);
        assertEquals( 1, getErrorSize(grade));
        assertEquals("may not be null", getErrorMessage(grade));
    }

    @Test
    public void gradeShouldNotBeValidWithInvalidExamTwo() {
        Grade grade = GradeFactory.validGrade();
        grade.setExamTwo(new BigDecimal(-5.00));
        assertEquals( 1, getErrorSize(grade));
        assertEquals("must be greater than or equal to 0.0", getErrorMessage(grade));

        grade.setExamTwo(new BigDecimal(10.00001));
        assertEquals( 1, getErrorSize(grade));
        assertEquals("must be less than or equal to 10.0", getErrorMessage(grade));
    }
}
